import gym
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import matplotlib.pyplot as plt



class Agent():

    #évaluation de la performance de l'agent à travers un plot des gains cumulés par episode en fonction du nbre d'interactions
    def plot_evolution(x, scores,filename, lines=None):
        fig = plt.figure()
        ax=fig.add_subplot(111, label="1")
        ax.plot(x, scores, color="C0")
        ax.set_xlabel("Jeux", color="C0")
        ax.set_ylabel("Gains moyens", color="C0")
        ax.tick_params(axis='x', colors="C0")
        ax.tick_params(axis='y', colors="C0")
        if lines is not None:
            for line in lines:
                plt.axvline(x=line)

        plt.savefig(filename)

#initialisation de l'environnement
    env = gym.make('CartPole-v1')
    env.reset(seed=0)

    # screen video des performances de l'agent en utilisant Videorecorder
    video_recorder = None
    video_recorder = VideoRecorder(env,'./Agent_aléatoire_enregistrement.mp4', enabled=True)
    episodes = [] # l'ensemble des épisodes
    cumul_rewards = [] #liste des cumuls des récompenses
    for episode in range(50):
        state = env.reset()
        inter_num = 0
        rewards = 0
        while True: 
            env.render()
            video_recorder.capture_frame()
            action = env.action_space.sample()
            state, reward, done, truncated, info = env.step(action)
            nbre_interaction = nbre_interaction + 1
            rewards = rewards + reward
            if done:
                break
        print("Episode : " + str(episode) + " ; nombre  d'intéractions : " + str(nbre_interaction) + " ; les récompenses : " +str(rewards))
        cumul_rewards.append(rewards)
        episodes.append(episode)


    print("Saved video.")
    video_recorder.close()
    video_recorder.enabled = False
    filename = 'Agent_aléatoire.png'
    plot_evolution(episodes,cumul_rewards, filename)
    env.close()



def main():

    agent = Agent()
    
if __name__ == '__main__':
    main()





