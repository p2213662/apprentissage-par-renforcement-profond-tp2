gym==0.21.0
importlib-metadata==4.8.1
importlib-resources==5.4.0
ipython==7.28.0
ipython-genutils==0.2.0
make==0.1.6.post2
matplotlib==3.4.3
pyinstaller==4.6
torch==1.10.0
torchvision==0.11.1


