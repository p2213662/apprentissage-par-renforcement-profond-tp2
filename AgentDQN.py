import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.transforms as T
import os
import random
import gym
import pylab
import numpy as np
from collections import deque
# from gym.wrappers import Monitor
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import time
from matplotlib import pyplot as plt

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class DQNAgent(object):

    def __init__(self, env_name, model, layer_size, batch_size=128, gamma=0.95, memory_size=1e4):
        # les parametres du modele :
        self.nbre_freq = 2000
        self.env_name = env_name

        self.env = self.wrap_env(gym.make(env_name))
        self.env.seed(0)

        # by default, CartPole-v1 has max episode steps = 500
        # we can use this to experiment beyond 500
        self.env._max_episode_steps = 4000
        self.state_size = self.env.observation_space.shape[0]
        self.action_size = self.env.action_space.n
        self.EPISODES = 2000
        self.model = network_model(layer_size)
        self.optimizer = optim.RMSprop(self.model.parameters(), lr=0.00025, eps=0.01, alpha=0.95)
        self.criterion = nn.MSELoss()
        self.loss = 0
        ## EXPLORATION HYPERPARAMETERS for epsilon and epsilon greedy strategy
        self.epsilon = 1.0  # exploration probability at start
        self.epsilon_min = 0.01  # minimum exploration probability
        self.epsilon_decay = 0.0005  # exponential decay rate for exploration prob

        # Instantiate memory
        memory_size = 10000
        self.memory = deque(maxlen=65536)
        self.epsilon_greedy = False  # use epsilon greedy improved strategy

        self.Save_Path = 'Models'
        if not os.path.exists(self.Save_Path): os.makedirs(self.Save_Path)
        self.scores, self.episodes, self.average = [], [], []

        self.Model_name = os.path.join(self.Save_Path, self.env_name + "_PER_D3QN_CNN.h5")
    #storing the entry tuple in the agent memory
    def remember(self, state, action, reward, next_state, done):
        experience = state, action, reward, next_state, done
        self.memory.append((experience))

    # picking randomly a sample from the deque memory
    def sampling_minibatch(self, memory, batch_size):
        # Randomly sample minibatch from the deque memory
        minibatch = random.sample(memory, min(len(memory), batch_size))

        state = np.zeros([batch_size, 4])
        next_state = np.zeros([batch_size, 4])
        action, reward, done = [], [], []
        for i in range(len(minibatch)):
            state[i] = minibatch[i][0]
            action.append(minibatch[i][1])
            reward.append(minibatch[i][2])
            next_state[i] = minibatch[i][3]
            done.append(minibatch[i][4])

        return state, next_state, action, reward, done
    #agent appropriating the epsilon greedy strategy to make decision related to his actions in the environment
    def act(self, state, decay_step):

        # EPSILON GREEDY STRATEGY
        if self.epsilon_greedy:
            # Here we'll use an improved version of our epsilon greedy strategy for Q-learning
            explore_probability = self.epsilon_min + (self.epsilon - self.epsilon_min) * np.exp(
                -self.epsilon_decay * decay_step)
        # OLD EPSILON STRATEGY
        else:
            if self.epsilon > self.epsilon_min:
                self.epsilon *= (1 - self.epsilon_decay)
            explore_probability = self.epsilon

        if explore_probability > np.random.rand():
            # Make a random action (exploration)
            q = random.randrange(self.action_size)
            return int(np.argmax(q)), explore_probability

        else:
            # Get action from Q-network (exploitation)
            # Estimate the Qs values state
            # Take the biggest Q value (= the best action)
            q = self.model(torch.from_numpy(state).float().unsqueeze(0).to(device))
            return int(torch.argmax(q)), explore_probability


    def train(self, memory, batch_size):
        if self.apprentissage_counter == self.nbre_freq:
            self.target.load_state_dict(self.model.state_dict())
            self.apprentissage_counter = 0

        obs, actions, next_obs, rewards, dones = self.sampling_minibatch(memory, batch_size)
        ml = self.model(obs)
        predicted_qv = torch.gather(ml, 1, actions.unsqueeze(-1)).squeeze()

        with torch.no_grad():
            target_ml = self.target(next_obs)
        predicted_target_qv, _ = torch.max(target_ml, 1)
        target_gain = []
        rewards_arr = rewards.tolist()
        predicted_target_qv_arr = predicted_target_qv.tolist()
        for i, done in enumerate(dones):
            target_gain.append(rewards_arr[i])
            if not done:
                target_gain[i] += self.gamma * predicted_target_qv_arr[i]
        loss = self.loss_funct(predicted_qv, torch.tensor(target_gain))
        self.optim.zero_grad()
        loss.backward()
        self.optim.step()
        self.apprentissage_counter += 1



    pylab.figure(figsize=(18, 9))

    def PlotModel(self, score, episode):
        self.scores.append(score)
        self.episodes.append(episode)
        self.average.append(sum(self.scores[-50:]) / len(self.scores[-50:]))
        pylab.plot(self.episodes, self.average, 'r')
        pylab.plot(self.episodes, self.scores, 'b')
        pylab.ylabel('Rewards Avrg', fontsize=18)
        pylab.xlabel('Game', fontsize=18)
        greedy = ''
        if self.epsilon_greedy: greedy = '_Greedy'
        try:
            pylab.savefig(self.env_name + greedy + "_DQN.png")
        except OSError:
            pass

        return str(self.average[-1])[:5]

    def reset_function(self):
        self.env.reset()
        return np.array([0, 0, 0, 0])

    def step_function(self, action):
        next_state, reward, done, info = self.env.step(action)
        return next_state, reward, done, info

    def run(self):
        decay_step = 0
        loss_tab = []
        dd = 0
        dd_old = 0
        for e in range(self.EPISODES):
            state = self.reset_function()
            done = False
            i = 0
            while not done:
                decay_step += 1
                action, explore_probability = self.act(state, decay_step)
                next_state, reward, done, _ = self.step_function(action)

                if not done or i == self.env._max_episode_steps - 1:
                    reward = reward
                else:
                    reward = -1e2
                self.remember(state, action, reward, next_state, done)
                state = next_state
                i += 1
                if done:
                    if e % 10 == 0:
                        self.train(self, self.memory, self.batch_size)
                    average = self.PlotModel(i, e)
                    loss_tab.append(self.loss)
                    print("episode: {}/{}, score: {}, e: {:.2}, average: {}\n".format(e, self.EPISODES, i,
                                                                                      explore_probability, average))
                    if i == self.env._max_episode_steps:
                        print("Saving trained model to", self.Model_name)
                        break
                self.train(self.batch_size)
        plt.plot(loss_tab)

    def wrap_env(self, env):
        # env = Monitor(env, './video', force=True)
        env = VideoRecorder(env, './Agent_aléatoire_enregistrement.mp4', enabled=True)
        return env




def network_model(layer_size):
    model = []
    nbl = len(layer_size) - 1
    for i in range(nbl):
        linear = torch.nn.Linear(layer_size[i], layer_size[i + 1])
        model.append(linear)
        if i < nbl - 1:
            fct_activation = torch.nn.ReLU()
            model.append(fct_activation)
    return torch.nn.Sequential(*model)

def save_model(model):
    torch.save(model.state_dict(), 'model_saved_DQN.pth')

def load_model(layer_size):
    model = network_model(layer_size)
    model.load_state_dict(torch.load('model_saved_DQN.pth'))
    model.eval()
    return model

# notre agent est entrainé :

env_name = 'CartPole-v1'

agent = DQNAgent(env_name, model=network_model([4, 32, 32, 2]), layer_size=[4, 32, 32, 2])

agent.run()
